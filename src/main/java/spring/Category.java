package spring;

import org.springframework.context.annotation.Configuration;

@Configuration
public class Category {
 
	public String name;
	public String toString () {
		return "Category[name="+name+"]";
	}
}
